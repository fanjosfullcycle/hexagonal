package app_test

import (
	"hexagonal/app"
	"testing"
	"github.com/stretchr/testify/require"
	uuid "github.com/satori/go.uuid"
)

func TestProduct_Enable(t *testing.T){
	product := app.Product{}
	product.Name = "Hello"
	product.Status = app.DISABLED
	product.Price = 10

	err := product.Enable()
	require.Nil(t, err)

	product.Price = 0
	err = product.Enable()
	require.Equal(t, "The price must be greater then zero to enable the product!", err.Error())
}

func TextProduct_Disable(t *testing.T){
	product := app.Product{}
	product.Name = "Hello"
	product.Status = app.ENABLED
	product.Price = 0

	err := product.Disable()
	require.Nil(t, err)

	product.Price = 10
	err = product.Disable()
	require.Equal(t, "The price must be zero in order to have the product disabled!", err.Error())
}

func TestProduct_IsValid(t *testing.T){
	product := app.Product{}
	product.Id = uuid.NewV4().String()
	product.Name = "hello"
	product.Status = app.DISABLED
	product.Price = 10

	_, err := product.IsValid()
	require.Nil(t, err)

	product.Status = "INVALID"
	_, err = product.IsValid()
	require.Equal(t, "The status must be enabled or disabled", err.Error())

	product.Status = app.ENABLED
	_, err = product.IsValid()
	require.Nil(t, err)

	product.Price = -10
	_, err = product.IsValid()
	require.Equal(t, "The price must be greater or equal zero", err.Error())


}