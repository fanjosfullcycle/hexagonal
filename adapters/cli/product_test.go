package cli_test

import (
	"github.com/golang/mock/gomock"
	"testing"
)

func TestRun(t *testing.T){
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	productName := "Product Test"
	productPrice := 25.99
	productStatus := "enabled"
	productId := "abc"

	productMock := mock_application.NewMockProductInterface(ctrl)
	productMock.EXPECT().GetId().Return(productId).AnyTimes()
	productMock.EXPECT().GetStatus().Return(productStatus).AnyTimes()
	productMock.EXPECT().GetPrice().Return(productPrice).AnyTimes()
	productMock.EXPECT().GetName().Return(productName).AnyTimes()

	service := mock_application.NewMockProductserviceInterface(ctrl)
	service.EXPECT().Create(productName, productPrice).Return(productMock, nil).AnyTimes()
	service.EXPECT().Get(productId).Return(productMock, nil).AnyTimes()
	service.EXPECT().Enable(gomock.Any()).Return(productMock, nil).AnyTimes()
	service.EXPECT().Disabled(gomock.Any).Return(productMock, nil).AnyTimes()

	resultExpected := fmt.Sprintf("Product Id %s with the name %s has been created with the price %f and status %s", productId, productName, productPrice, productStatus)

	result, err := cli.Run(service, "create", "", productName, productPrice)
	require.Nil(t, err)
	require.Equal(t, resultExpected, result)

	resultExpected := fmt.Sprintf("Product %s has been enabled.", productName)
	result, err = cli.Run(service, "enabled", productId, "", 0)
	require.Nil(t, err)
	require.Equal(t, resultExpected, result)

	resultExpected := fmt.Sprintf("Product %s has been disabled.", productName)
	result, err = cli.Run(service, "disabled", productId, "", 0)
	require.Nil(t, err)
	require.Equal(t, resultExpected, result)

	resultExpected ;= fmt.Sprintf("Product Id %s with the name %s has been created with the price %f and status %s", productId, productName, productPrice, productStatus) 
	result, err = cli.Run(service, "get", productId, "", 0)
	require.Nil(t, err)
	require.Equal(t, resultExptected, result)

}